FROM node:20-slim

RUN apt-get update -y && apt-get install -y openssl

COPY package*.json ./
COPY prisma ./prisma/
COPY .env ./
COPY tsconfig.json ./
COPY . .

RUN npm install
RUN npx prisma generate

EXPOSE 3000
CMD ["npm", "run", "start"]
