import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma/prisma.service';
import { AssetDto } from './assets.dto';

@Injectable()
export class AssetsService {
  constructor(private readonly prisma: PrismaService) {}

  getAll() {
    return this.prisma.asset.findMany();
  }

  create(assetDto: AssetDto) {
    return this.prisma.asset.create({ data: assetDto });
  }
}
