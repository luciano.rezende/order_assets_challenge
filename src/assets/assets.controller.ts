import { Body, Controller, Get, Post } from '@nestjs/common';
import { AssetsService } from './assets.service';
import { AssetDto } from './assets.dto';

@Controller('assets')
export class AssetsController {
  constructor(private readonly assetsService: AssetsService) {}

  @Get()
  all() {
    return this.assetsService.getAll();
  }

  @Post()
  create(@Body() assetDto: AssetDto) {
    return this.assetsService.create(assetDto);
  }
}
