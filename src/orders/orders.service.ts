import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma/prisma.service';
import { OrderDto } from './orders.dto';
import { OrderStatus } from '@prisma/client';

@Injectable()
export class OrdersService {
  constructor(private readonly prisma: PrismaService) {}

  getAll() {
    return this.prisma.order.findMany({
      include: {
        Asset: {
          select: { symbol: true },
        },
      },
    });
  }

  createOrder(order: OrderDto) {
    return this.prisma.order.create({
      data: {
        asset_id: order.asset_id,
        price: order.price,
        status: OrderStatus.pending,
      },
    });
  }
}
